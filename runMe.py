import time
import sys
sys.path.insert(0, '/afs/cern.ch/user/m/mhoppesc/pPbFlow/ppbflowanalysis/python')
start = time.time()
print("Starting Execution")
# here we load the shared library created using the Makefile
from ROOT import gSystem
gSystem.Load("Analysis.so")
# now we can create instance of the class EventLoop
from Samples import *
#eventLoop = SampleGridTestv3()
#eventLoop = SampleSmall()
eventLoop = SampleMinBias()
#eventLoop = SampleJetTriggers()
# create algorithm
from Algorithms import *
algs = []
algs += [ AlgDF() ]
# add the algorithm into the event loop
eventLoop.addAlgorithms( algs )
endInit = time.time()
print("InitilizationTime = {t}".format(t=endInit-start))
# initialize and execute the event loop
eventLoop.execute()
endLoop = time.time()
print("RunTime = {t}".format(t=endLoop - endInit))
# save plots from all algorithms in the loop
eventLoop.save()
endSave = time.time()
print("RunTime = {t}".format(t=endSave - endLoop))
