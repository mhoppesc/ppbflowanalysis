import sys
sys.path.insert(0, '/Users/mhoppesch/jetsResarch/pPbAnalysisHistograms/python')
import time

start = time.time()
print("Starting Execution")
# here we load the shared library created using the Makefile
from ROOT import gSystem
gSystem.Load("Analysis.so")

# now we can create instance of the class EventLoop
from CalibSamples import *
calibLoops = []
#calibLoops += [SampleJetTriggers()]
#calibLoops += [SampleSmall()]
#calibLoops += [SampleMinBias()]
calibLoops += [SampleGridTestv3()]
endInit = time.time()
print("InitilizationTime = {t}".format(t=endInit-start))
# initialize and execute the event loop
for calibLoop in calibLoops:
  calibLoop.execute()
endLoop = time.time()
print("RunTime = {t}".format(t=endLoop - endInit))

