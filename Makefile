#-----------------------------------------------------------------------------

PROGRAM = Analysis

HDRS = include/EventLoop.h\
			 include/CalibrationLoop.h\
			 include/Data.h\
			 include/tree.h\
			 include/Algorithm.h\
			 include/TrackCalibrationWeights.h\
			 include/Harmonic.h\
			 include/FlowEvent.h\
			 include/MeanQvector.h\
			 include/QCalibrationMeans.h\
			 include/Resolution.h\

OBJS = src/EventLoop.o\
			src/CalibrationLoop.o\
			src/Data.o\
			src/tree.o\
			src/Algorithm.o\
			src/TrackCalibrationWeights.o\
			src/Harmonic.o\
			src/FlowEvent.o\
			src/MeanQvector.o\
			src/QCalibrationMeans.o\
			src/Resolution.o\
        dict_$(PROGRAM).o

#-----------------------------------------------------------------------------
ObjSuf = o
SrcSuf = cpp
ExeSuf = exe
DllSuf = so
OutPutOpt = -o

ROOTCFLAGS = $(shell root-config --cflags)
ROOTLIBS = $(shell root-config --libs)
ROOTGLIBS = $(shell root-config --glibs)

# Directories
HEADRDIR = .

# debug option: -g
# no exceptions: -fno-exceptions

CXX = g++
CXXFLAGS = -O3 -Wall -fPIC -g -std=c++11
LD = g++
LDFLAGS = -O3 -Wall -g
SOFLAGS = -shared
ARCHFLAGS =

CXXFLAGS += $(ROOTCFLAGS) -I$(HEADRDIR)  
LIBS = $(ROOTLIBS)
GLIBS = $(ROOTGLIBS)


%/%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $*.cpp

$(PROGRAM).so: $(OBJS)
	@echo "Linking $(PROGRAM).so ..."
	$(LD) $(ARCHFLAGS) $(LDFLAGS) $(SOFLAGS) $(OBJS) $(ROOTLIBS)  -o$(PROGRAM).so 
	@echo "linking done"

dict_$(PROGRAM).cpp: $(HDRS)
	@echo "Generating dictionary ..."
	rootcint -f dict_$(PROGRAM).cpp -c -p -I$(HEADRDIR) $(HDRS) Linkdef.h

clean:
	@rm -f $(OBJS) $(PROGRAM).so dict_*


#------------------------------------------------------------------------
