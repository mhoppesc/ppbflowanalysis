from Plot import Plot
from ROOT import TCanvas, TLegend, kRed, kGreen, kBlack

# set ATLAS plot style
from AtlasStyle import setStyle
setStyle()
kGreen = 416
kRed = 632
kBlue = 600
kMagenta = 616
kCyan = 432
histName =
sampleNames = ['PbPb2018AllVertex']

