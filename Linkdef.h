#ifdef __CINT__

#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

// includes all header files
#include "include/EventLoop.h"
#include "include/CalibrationLoop.h"
#include "include/Data.h"
#include "include/tree.h"
#include "include/Algorithm.h"
#include "include/TrackCalibrationWeights.h"
#include "include/Harmonic.h"
#include "include/FlowEvent.h"
#include "include/MeanQvector.h"
#include "include/QCalibrationMeans.h"
#include "include/Resolution.h"


// All classes
#pragma link C++ class EventLoop + ;
#pragma link C++ class CalibrationLoop + ; 
#pragma link C++ class Data + ;
#pragma link C++ class tree + ;
#pragma link C++ class Algorithm + ;
#pragma link C++ class TrackCalibrationWeights + ;
#pragma link C++ class QCalibrationMeans + ;
#pragma link C++ class Resolution + ;
#pragma link C++ class map < int, int>;

// all functions
#pragma link C++ function runMe;
#pragma link C++ function plotMe;
#pragma link C++ function calibrateMe.py;
#endif
