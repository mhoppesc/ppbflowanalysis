#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "TString.h"
#include "TH1D.h"
#include "TH1F.h"
#include "TH2F.h"
#include "Data.h"
#include "Harmonic.h"
#include "MeanQvector.h"
#include "TrackCalibrationWeights.h"
#include "Resolution.h"
#include "TVector.h"
#include <iostream>
#include <stdexcept>


class Algorithm
{
public:
  /**
   * @brief Construct a new Event Loop object
   */
  Algorithm();

  /**
   * @brief Initialize the algorithm
   * @param data - pointer to the data-access class instance
   */
  void initialize(Data *data, std::vector<Harmonic *> *harmonics,
                  std::vector<MeanQvector *> *meanQvectors,
                  std::vector<TrackCalibrationWeights*> *trackCalib,
                  std::vector<Resolution *> *resolutions);

  /**
   * @brief ExecuteData. Here the stuff happens
   */
  void executeData();
  /**
   * @brief ExecuteHarmonic. Here the stuff happens
   */
  void executeHarmonic();
  /**
   * @brief ExecuteHarmonic. Here the stuff happens
   */
  void executeMeanQvectors();
  /**
   * @brief Pointer to the histogram class instance defined as public attribute
   */
  void executeTrackCalib();
  
  void executeResolutions();

  //TH1D *h_v2_c0 = 0; // must be initialized to zero
  std::vector<TH1D > h_flowSP = std::vector<TH1D >(20);
  std::vector<TH1D> h_flowNum = std::vector<TH1D>(20);
  std::vector<TH1D>  h_rawHarmonicsNum;
  std::vector<TH1D > h_rawResolutions;
  std::vector<TH1D> h_rawQvecsRe = std::vector<TH1D>(5);
  std::vector<TH1D> h_rawQvecsIm = std::vector<TH1D>(5);
  std::vector<TH1D> h_calibQvecsRe = std::vector<TH1D>(5);
  std::vector<TH1D> h_calibQvecsIm = std::vector<TH1D>(5);
  std::vector<TH1F> h_meanCalibQvectorHistReal;// = std::vector<TH1F>(100);
  std::vector<TH1F> h_meanCalibQvectorHistImaginary;// = std::vector<TH1F>(100);
  std::vector<TH1F> h_meanRawQvectorHistReal;     // = std::vector<TH1F>(100);
  std::vector<TH1F> h_meanRawQvectorHistImaginary; // = std::vector<TH1F>(100);
  std::vector<TH2D> h_trackWeights;
 
  /**
   * @brief Selection cuts
   */
  bool cut_selectNot1 = false;
  bool cut_select1 = false;

protected:
  /**
   * @brief Apply the selection
   *
   * @return true when event passed the selection.
   */
  bool passedSelectionHarmonic(int n, int centBin);

  /**
   * @brief Fill the histograms
   */
  void fillPlotsHarmonic(int n, int centBin, int bin, double meanSP, double errSP, double meanNum, double meanNumErr);
  void fillPlotsMeanQVectors(int n,bool isN,int bin,double meanCalibRe, double errorCalibRe,
                                  double meanCalibIm, double errorCalibIm, double meanRawRe,double meanRawIm);
  std::pair<double,double> computeVn(double Num, double NumErr, double Res, double ResErr);  //Denomiator of Vn calculation is reffered to as the resolution
  /**
   * @brief Instance of the data-access class
   */
  Data *m_data = 0;
  /**
   * @brief Instance of the harmonics-access class
   */
  std::vector<Harmonic * > *m_harmonics = 0;
  /**
   * @brief Instance of the mean Q vectors-access class
   */
  std::vector<MeanQvector *> *m_meanQvectors = 0;
  /**
   * @brief Instance of the mean Q vectors-access class
   */
  std::vector<TrackCalibrationWeights*> *m_trackCalib;
  /**
   * @brief Instance of the Resolution-access class
   */
  std::vector<Resolution *> *m_resolutions;

  int m_num_harmonics;
  int m_num_VnCentBins;
};

#endif
