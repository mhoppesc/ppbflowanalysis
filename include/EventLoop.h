#ifndef EVENTLOOP_H
#define EVENTLOOP_H

#include <vector>
#include <TString.h>
#include <TChain.h>
#include "TH2D.h"
#include "Data.h"
#include "Algorithm.h"
#include "TrackCalibrationWeights.h"
#include "Harmonic.h"
#include "FlowEvent.h"
#include "MeanQvector.h"
#include "QCalibrationMeans.h"
#include "Resolution.h"

class EventLoop
{
public:
  /**
   * @brief Construct a new Event Loop object
   */
  EventLoop() =default;

  /**
   * @brief Initialize the event loop
   */
  void initialize();

  /**
   * @brief Execute the event loop
   */
  void execute();
  /**
   * @brief list of input ROOT file names
   */
  std::vector<TString> inputFiles;

  /**
   * @brief Name of the TTree instance. Must be same in all files
   */
  TString treeName;
  
  TString calibFileName;
  /**
   * @breif Map that keeps track of which Runs are pPb(ions travel toward positive rapidity) and which are Pbp, true is pPb (ions travel toward negative rapidity) false is Pbp
   */
  std::map<int,bool> runDirection;

  /**
   * @brief List of algorithms to be executed in the event loop
   */
  std::vector<Algorithm *> algorithms;
protected:
  /**
   * @brief Instance of the TChain class used to read the data
   */
  TChain *m_chain = 0;

  /**
   * @brief Instance of the data-access class
   */
  Data *m_data = 0;
  std::vector<TrackCalibrationWeights*>* m_weights=0;
  std::vector<Resolution*>* m_resolutions = 0;
  std::vector<QCalibrationMeans*> m_qCalibMeans;
  std::vector<Harmonic*> *m_harmonics = 0;
  std::vector<MeanQvector*> *m_meanQvectors = 0;
  std::map<int, int> m_runMap;
  bool m_ionDirection;
  std::vector<float> pt_range;
  std::vector<float> eta_range;
  std::vector<float> phi_range;
  int m_num_harmonics;
  int m_num_ptBins;
  int m_num_centralityBins;
  int m_num_EtaDivs;
  int m_num_VnCentralityBins;
  std::map<int, int> m_centMap;

};

#endif
