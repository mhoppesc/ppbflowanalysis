#ifndef HARMONIC_H
#define HARMONIC_H

#include "TTree.h"
#include "vector"
#include "TComplex.h"
#include "TH1D.h"

class Harmonic
{
public:
  /**
   * @brief Construct a new Harmonic object
   *
   * @param tree - pointer to the TTree (or TChain) class
   */
  Harmonic(int n, int ptBin, int m_centralityBin);

  /**
   * @brief Tree variables
   */
  void AddHarmonicEP(TComplex bigQ_n);
  void AddHarmonic(TComplex bigQ_n, float phi, float weight,float Normalization);
  TComplex smallQ(float phi, float weight);
  double getNumVn() { return m_harmonicHist->GetMean(); }
  double getNumVnErr() { return m_harmonicHist->GetMean(11); }
  double getResVn(){return m_resolutionHist->GetMean();}
  double getResVnErr() {return m_resolutionHist->GetMean(11);}
  double getVnEP() {return m_harmonicHistEP->GetMean(); }
  double debugHarm() {return m_lastHarm;}
  TH1D getHistEP() {return *m_harmonicHistEP;}
  TH1D getHist() {return *m_harmonicHist;}
  TH1D getHistRes() { return *m_resolutionHist; }
  void buffEmpty() {m_harmonicHist->BufferEmpty(-1);
  m_resolutionHist->BufferEmpty(-1);}
  void resetRange() {m_harmonicHist->GetXaxis()->SetRange(0,0);
    m_resolutionHist->GetXaxis()->SetRange(0, 0);
  } // ensure numerical calculation of mean
  int getN(){return m_n;}
  int getptBin(){return m_ptBin;}
  int getcentBin(){return m_centralityBin;}
  double m_numeratorSum; // tcomplex to perfomr event averaging
  int m_numEventCounter;   // counter for number of events
protected:
  int m_n;                  // n-th harmonic
  int m_ptBin;              // which ptBin?
  int m_centralityBin;      // which centrality bin
  double m_lastHarm;
  TH1D* m_harmonicHist; // container to store the harmonic measurments ()
  TH1D* m_resolutionHist;
  TH1D* m_harmonicHistEP;
};

#endif
