#ifndef CALIBRATIONLOOP_H
#define CALIBRATIONLOOP_H

#include <vector>
#include <TString.h>
#include <TChain.h>
#include "TH2D.h"
#include "Data.h"
#include "Algorithm.h"
#include "TrackCalibrationWeights.h"
#include "Harmonic.h"
#include "FlowEvent.h"
#include "MeanQvector.h"
#include "QCalibrationMeans.h"
#include "TMap.h"

class CalibrationLoop
{
public:
  /**
   * @brief Construct a new Calibration Loop object
   */
  CalibrationLoop() = default;

  /**
   * @brief Initialize the Calibration loop
   */
  void initialize();

  /**
   * @brief Execute the Calibration loop
   */
  void execute();
  /**
   * @brief list of input ROOT file names
   */
  std::vector<TString> inputFiles;

  /**
   * @brief Name of the TTree instance. Must be same in all files
   */
  TString treeName;

  /**
   * @brief Name of the TTree instance. Must be same in all files
   */
  TString calibFileName;

  /**
   * @breif Map that keeps track of which Runs are pPb(ions travel toward positive rapidity) and which are Pbp, true is pPb (ions travel toward negative rapidity) false is Pbp
   */
  std::map<int, bool> runDirection;

  /**
   * @brief List of algorithms to be executed in the Calibration loop
   */
  std::vector<Algorithm *> algorithms;

protected:
  /**
   * @brief Instance of the TChain class used to read the data
   */
  TChain *m_chain = 0;

  /**
   * @brief Instance of the data-access class
   */
  Data *m_data = 0;
  std::vector<TrackCalibrationWeights* > m_weights;
  std::vector<QCalibrationMeans *> m_qCalibMeans;
  std::map<int, int> m_runMap;
  int m_num_centralityBins;
  int m_num_harmonics;
  int runNum;

};

#endif
