#ifndef RESOLUTION_H
#define RESOLUTION_H

#include "TTree.h"
#include "vector"
#include "TComplex.h"
#include "TH1D.h"

class Resolution
{
public:
  /**
   * @brief Construct a new Harmonic object
   *
   * @param tree - pointer to the TTree (or TChain) class
   */
  Resolution(int n, int m_centralityBin);

  /**
   * @brief Tree variables
   */
  void AddRes(float Normalization);

  double getRes() { return m_resolutionHist->GetMean(); }
  double getResErr() { return m_resolutionHist->GetMean(11); }

  TH1D getHistRes() { return *m_resolutionHist; }
  void buffEmpty()
  {
    m_resolutionHist->BufferEmpty(-1);
  }
  void resetRange()
  {
    m_resolutionHist->GetXaxis()->SetRange(0, 0);
  } // ensure numerical calculation of mean
  int getN() { return m_n; }
  int getcentBin() { return m_centralityBin; }
  double m_numeratorSum; // tcomplex to perfomr event averaging
  int m_numEventCounter; // counter for number of events
protected:
  int m_n;             // n-th harmonic
  int m_centralityBin; // which centrality bin
  double m_lastHarm;
  TH1D *m_resolutionHist;
};

#endif
