#ifndef MEANQVECTOR_H
#define MEANQVECTOR_H

#include "TTree.h"
#include "vector"
#include "TComplex.h"
#include "TH1F.h"

class MeanQvector
{
public:
  /**
   * @brief Construct a new Harmonic object
   *
   * @param tree - pointer to the TTree (or TChain) class
   */
  MeanQvector(int n, int centralityBin, bool etaSide);

  /**
   * @brief Tree variables
   */
  void AddToAverage(TComplex raqQ, TComplex calibQ);
  double getCalibMeanQRe() {  return m_meanCalibQvectorHistReal->GetMean(); }
  double getCalibMeanQReErr() { return m_meanCalibQvectorHistReal->GetStdDev(); }
  double getCalibMeanQIm() { return m_meanCalibQvectorHistImaginary->GetMean(); }
  double getCalibMeanQImErr() { return m_meanCalibQvectorHistImaginary->GetStdDev(); }
  double getRawMeanQRe() { return m_meanRawQvectorHistReal->GetMean(); }
  double getRawMeanQReErr() { return m_meanRawQvectorHistReal->GetStdDev(); }
  double getRawMeanQIm() { return m_meanRawQvectorHistImaginary->GetMean(); }
  double getRawMeanQImErr() {  return m_meanRawQvectorHistImaginary->GetStdDev(); }
  TH1F get_meanCalibQvectorHistReal() { return *m_meanCalibQvectorHistReal;}

  TH1F get_meanCalibQvectorHistImaginary() { return *m_meanCalibQvectorHistImaginary; }

  TH1F get_meanRawQvectorHistReal() { return *m_meanRawQvectorHistReal; }

  TH1F get_meanRawQvectorHistImaginary() { return *m_meanRawQvectorHistImaginary; }
  int getN() { return m_n; }
  int getcentBin() { return m_centralityBin; }
  bool getIsN() {return m_isN; }
  
protected:
  int m_n;             // n-th harmonic
  int m_centralityBin; // which centrality bin
  bool m_isN;        //
  TH1F *m_meanCalibQvectorHistReal;      // container to store the calibrated qvector measurments ()
  TH1F *m_meanCalibQvectorHistImaginary; //
  TH1F *m_meanRawQvectorHistReal;      // container to store the raw qvector measurments ()
  TH1F *m_meanRawQvectorHistImaginary; //
};

#endif
