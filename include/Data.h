#ifndef DATA_H
#define DATA_H

#include "TTree.h"
#include "vector"

class Data
{
public:
  /**
   * @brief Construct a new Data object
   *
   * @param tree - pointer to the TTree (or TChain) class
   */
  Data(TTree *tree);
  /**
   * @brief Tree variables
   */
  
  
  float FCal_Et;
  float FCal_Et_P;
  float FCal_Et_N;
  unsigned int event_nTrk;
  unsigned int runNumber;
  unsigned int eventNumber;
  int centrality;
  std::vector<float> *FCal_Qx=0;
  std::vector<float> *FCal_Qy=0;
  std::vector<float> *track_eta = 0;
  std::vector<float> *track_phi=0;
  std::vector<float> *track_pt=0;

  /**
   * @brief Branch Variables
   */
  TBranch *b_FCal_Et;
  TBranch *b_FCal_Et_P;
  TBranch *b_FCal_Et_N;
  TBranch *b_event_nTrk;
  TBranch *b_runNumber;
  TBranch *b_eventNumber;
  TBranch *b_centrality;
  TBranch *b_FCal_Qx;
  TBranch *b_FCal_Qy;
  TBranch *b_track_eta;
  TBranch *b_track_phi;
  TBranch *b_track_pt;
  
  void ResetBranch(TTree *tree);

protected:
  /**
   * @brief pointer to the TTree (or TChain) class
   */
  TTree *m_tree = 0;
};

#endif
