#ifndef TRACKCALIBRATIONWEIGHTS_H
#define TRACKCALIBRATIONWEIGHTS_H

#include "TH2D.h"
#include "TH1D.h"
#include "vector"

class TrackCalibrationWeights : public TObject
{
  
  public :
      /**
       * @brief Construct a new Data object
       *
       * @param
       */

  TrackCalibrationWeights(UInt_t RunN = 0);
  TrackCalibrationWeights(const TrackCalibrationWeights &w)
  {
    m_runNumber = w.m_runNumber;
    m_numEventsRunCounter = w.m_numEventsRunCounter;
    m_numBinsTrkWgt = w.m_numBinsTrkWgt;
    m_limitBinsTrkWgt = w.m_limitBinsTrkWgt;
    TH1::AddDirectory(kFALSE);
    m_trackWeight2D = static_cast<TH2D *>(w.m_trackWeight2D->Clone());
    m_trackWeight1D = static_cast<TH1D *>(w.m_trackWeight1D->Clone());
    m_trackWeightFinal = static_cast<TH2D*>(w.m_trackWeightFinal->Clone());
    m_trackMapCorrected = static_cast<TH2D *>(w.m_trackMapCorrected->Clone());
    }
  /**
   * @brief Tree variables
   */
  void AddElements(std::vector<float> *trk_eta, std::vector<float> *trk_phi);
  void FinalizeElements();
  float trkWgt(int psi_bin, int eta_bin){return m_trackWeightFinal->GetBinContent(psi_bin+1,eta_bin+1);} //do this to get the correct binning, root starts with 1 for th1 but 0 for th2
  UInt_t getRunNumber(){return m_runNumber;}
  TH2D trackWeight(){return *m_trackWeightFinal;}
  TH2D trackMapInit() { return *m_trackWeight2D; }
  TH2D trackMapCorrected() { return *m_trackMapCorrected; }
  TH2D* testPtr(){return m_trackWeight2D;}


protected:
  /**
   * @brief pointer to the TTree (or TChain) class
   */
  UInt_t m_runNumber;
  UInt_t m_numEventsRunCounter;
  TH2D* m_trackWeight2D = 0;
  TH1D* m_trackWeight1D = 0;
  TH2D* m_trackWeightFinal = 0;
  TH2D* m_trackMapCorrected = 0;
  std::pair<Int_t,Int_t> m_numBinsTrkWgt; //first is phi bins, second is eta bins
  std::pair<Float_t, Float_t> m_limitBinsTrkWgt; // first is phi bins limits (pi), second is eta bin limits
  
  
  ClassDef(TrackCalibrationWeights,1);
};

#endif
