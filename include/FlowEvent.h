#ifndef FLOWEVENT_H
#define FLOWEVENT_H

#include "TTree.h"
#include "vector"
#include "TComplex.h"
#include "TMath.h"
#include <math.h>

class FlowEvent
{


public:
  /**
   * @brief Construct a new FlowEvent Object, Stores all the event - by - event info one could need
   *
   * @param tree - pointer to the TTree (or TChain) class
   */

  //FlowEvent(UInt_t runNumber, std::vector<float> *FCal_Qx_Raw, std::vector<float> *FCal_Qy_Raw, std::vector<float> *FCal_Qx_Mean, std::vector<float> *FCal_Qy_Mean, float FCal_Et_P, float FCal_Et_N);
  int m_num_flowHarmonics;

  
  FlowEvent(int numFlowHarmonics) : m_num_flowHarmonics(numFlowHarmonics),
                                    m_Q_P(numFlowHarmonics),
                                    m_Q_N(numFlowHarmonics),
                                    m_Qraw_P(numFlowHarmonics),
                                    m_Qraw_N(numFlowHarmonics),
                                    m_resolutionEstimate(numFlowHarmonics)
                                    {}
  /**
   * @brief Tree variables
   */
  
  void ConstructFlowEvent(UInt_t runNumber, std::vector<float> *FCal_Qx_Raw, std::vector<float> *FCal_Qy_Raw, std::vector<float> *FCal_Qx_Mean, std::vector<float> *FCal_Qy_Mean, float FCal_Et_P, float FCal_Et_N);
  TComplex getCorrectedQVector(int n, bool isN);
  TComplex getRawQVector(int n, bool isN);
  TComplex getCorrectedQVectorConj(int n, bool isN); // These functions automatically conjugate the P side
  TComplex getRawQVectorConj(int n, bool isN);
  double    getResolutionEstimate(int n);

protected:
  UInt_t m_runNumber;
  std::vector<TComplex> m_Q_P;              // P is side A
  std::vector<TComplex> m_Q_N;              // N is side C
  std::vector<TComplex> m_Qraw_P;           // P is side A
  std::vector<TComplex> m_Qraw_N;           // N is side C
  std::vector<double> m_resolutionEstimate; // estimated detector resolution
};

#endif
