//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Feb 28 20:15:19 2023 by ROOT version 6.26/06
// from TTree AntiKt4HI/AntiKt4HI
// found on file: outputfile_pPb_2016_data.root
//////////////////////////////////////////////////////////
#ifndef tree_h
#define tree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"


class AntiKt4HI {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   std::vector<float> *HLT_jet_Et;
   std::vector<float> *HLT_jet_eta;
   std::vector<float> *HLT_jet_phi;
   std::vector<float> *L1_jet_E;
   std::vector<float> *L1_jet_eta;
   std::vector<float> *L1_jet_phi;
   Double_t MC_weight;
   Int_t event_Centrality;
   Float_t event_EdgeGapNeg;
   Float_t event_EdgeGapPos;
   Float_t event_Et;
   UInt_t event_EventNumber;
   Float_t event_FCal_Et;
   Float_t event_FCal_Et_A;
   Float_t event_FCal_Et_C;
   std::vector<float> *event_FCal_Qx;
   std::vector<float> *event_FCal_Qy;
   UInt_t event_RunNumber;
   Int_t event_Status_ZDC_A;
   Int_t event_Status_ZDC_C;
   Float_t event_SumGapsNeg;
   Float_t event_SumGapsPos;
   Float_t event_ZDC_A;
   Float_t event_ZDC_C;
   std::vector<float> *event_ZDC_modules;
   std::vector<float> *event_ZDC_modules_status;
   UInt_t event_lbn;
   Double_t event_mu;
   UInt_t event_nTrk;
   UInt_t event_npv;
   std::vector<float> *event_track_eta;
   std::vector<float> *event_track_phi;
   std::vector<float> *event_track_pt;
   std::vector<float> *event_v_pv_z;
   std::vector<float> *event_v_vtx_tracks;
   std::vector<float> *jet_E;
   std::vector<float> *jet_E_unsubtracted;
   std::vector<float> *jet_eta;
   std::vector<float> *jet_eta_emscale;
   std::vector<float> *jet_eta_unsubtracted;
   std::vector<bool> *jet_isGood;
   std::vector<float> *jet_m;
   Int_t jet_n;
   std::vector<float> *jet_phi;
   std::vector<float> *jet_pt;
   std::vector<float> *jet_pt_emscale;
   std::vector<float> *jet_pt_unsubtracted;
   std::vector<float> *jet_rtrk;
   std::vector<float> *jet_timing;
   std::vector<bool> *trigger_decisions;
   std::vector<bool> *trigger_decisions_emulated;
   std::vector<bool> *triggers_HLT_passedRAW;
   std::vector<bool> *triggers_HLT_prescaled;
   std::vector<bool> *triggers_HLT_rerun;
   std::vector<bool> *triggers_L1_passedAfterPrescale;
   std::vector<bool> *triggers_L1_passedAfterVeto;
   std::vector<bool> *triggers_L1_passedBeforePrescale;
   std::vector<float> *triggers_prescale;
   std::vector<float> *truth_jet_E;
   std::vector<float> *truth_jet_eta;
   std::vector<int> *truth_jet_flavor;
   std::vector<bool> *truth_jet_isIsolated;
   std::vector<float> *truth_jet_m;
   Int_t truth_jet_n;
   std::vector<float> *truth_jet_phi;
   std::vector<float> *truth_jet_pt;

   // List of branches
   TBranch *b_HLT_jet_Et;                       //!
   TBranch *b_HLT_jet_eta;                      //!
   TBranch *b_HLT_jet_phi;                      //!
   TBranch *b_L1_jet_E;                         //!
   TBranch *b_L1_jet_eta;                       //!
   TBranch *b_L1_jet_phi;                       //!
   TBranch *b_MC_weight;                        //!
   TBranch *b_event_Centrality;                 //!
   TBranch *b_event_EdgeGapNeg;                 //!
   TBranch *b_event_EdgeGapPos;                 //!
   TBranch *b_event_Et;                         //!
   TBranch *b_event_EventNumber;                //!
   TBranch *b_event_FCal_Et;                    //!
   TBranch *b_event_FCal_Et_A;                  //!
   TBranch *b_event_FCal_Et_C;                  //!
   TBranch *b_event_FCal_Qx;                    //!
   TBranch *b_event_FCal_Qy;                    //!
   TBranch *b_event_RunNumber;                  //!
   TBranch *b_event_Status_ZDC_A;               //!
   TBranch *b_event_Status_ZDC_C;               //!
   TBranch *b_event_SumGapsNeg;                 //!
   TBranch *b_event_SumGapsPos;                 //!
   TBranch *b_event_ZDC_A;                      //!
   TBranch *b_event_ZDC_C;                      //!
   TBranch *b_event_ZDC_modules;                //!
   TBranch *b_event_ZDC_modules_status;         //!
   TBranch *b_event_lbn;                        //!
   TBranch *b_event_mu;                         //!
   TBranch *b_event_nTrk;                       //!
   TBranch *b_event_npv;                        //!
   TBranch *b_event_track_eta;                  //!
   TBranch *b_event_track_phi;                  //!
   TBranch *b_event_track_pt;                   //!
   TBranch *b_event_v_pv_z;                     //!
   TBranch *b_event_v_vtx_tracks;               //!
   TBranch *b_jet_E;                            //!
   TBranch *b_jet_E_unsubtracted;               //!
   TBranch *b_jet_eta;                          //!
   TBranch *b_jet_eta_emscale;                  //!
   TBranch *b_jet_eta_unsubtracted;             //!
   TBranch *b_jet_isGood;                       //!
   TBranch *b_jet_m;                            //!
   TBranch *b_jet_n;                            //!
   TBranch *b_jet_phi;                          //!
   TBranch *b_jet_pt;                           //!
   TBranch *b_jet_pt_emscale;                   //!
   TBranch *b_jet_pt_unsubtracted;              //!
   TBranch *b_jet_rtrk;                         //!
   TBranch *b_jet_timing;                       //!
   TBranch *b_trigger_decisions;                //!
   TBranch *b_trigger_decisions_emulated;       //!
   TBranch *b_triggers_HLT_passedRAW;           //!
   TBranch *b_triggers_HLT_prescaled;           //!
   TBranch *b_triggers_HLT_rerun;               //!
   TBranch *b_triggers_L1_passedAfterPrescale;  //!
   TBranch *b_triggers_L1_passedAfterVeto;      //!
   TBranch *b_triggers_L1_passedBeforePrescale; //!
   TBranch *b_triggers_prescale;                //!
   TBranch *b_truth_jet_E;                      //!
   TBranch *b_truth_jet_eta;                    //!
   TBranch *b_truth_jet_flavor;                 //!
   TBranch *b_truth_jet_isIsolated;             //!
   TBranch *b_truth_jet_m;                      //!
   TBranch *b_truth_jet_n;                      //!
   TBranch *b_truth_jet_phi;                    //!
   TBranch *b_truth_jet_pt;                     //!

   AntiKt4HI(TTree *tree=0);
   virtual ~AntiKt4HI();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef AntiKt4HI_cxx
AntiKt4HI::AntiKt4HI(TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("outputfile_pPb_2016_data.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("outputfile_pPb_2016_data.root");
      }
      f->GetObject("AntiKt4HI",tree);

   }
   Init(tree);
}

AntiKt4HI::~AntiKt4HI()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t AntiKt4HI::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t AntiKt4HI::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void AntiKt4HI::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   HLT_jet_Et = 0;
   HLT_jet_eta = 0;
   HLT_jet_phi = 0;
   L1_jet_E = 0;
   L1_jet_eta = 0;
   L1_jet_phi = 0;
   event_ZDC_modules = 0;
   event_ZDC_modules_status = 0;
   event_q_N_Cos = 0;
   event_q_N_Sin = 0;
   event_q_P_Cos = 0;
   event_q_P_Sin = 0;
   event_v_pv_z = 0;
   event_v_vtx_tracks = 0;
   jet_E = 0;
   jet_E_unsubtracted = 0;
   jet_eta = 0;
   jet_eta_emscale = 0;
   jet_eta_unsubtracted = 0;
   jet_isGood = 0;
   jet_m = 0;
   jet_phi = 0;
   jet_pt = 0;
   jet_pt_emscale = 0;
   jet_pt_unsubtracted = 0;
   jet_rtrk = 0;
   jet_timing = 0;
   trigger_decisions = 0;
   trigger_decisions_emulated = 0;
   triggers_HLT_passedRAW = 0;
   triggers_HLT_prescaled = 0;
   triggers_HLT_rerun = 0;
   triggers_L1_passedAfterPrescale = 0;
   triggers_L1_passedAfterVeto = 0;
   triggers_L1_passedBeforePrescale = 0;
   triggers_prescale = 0;
   truth_jet_E = 0;
   truth_jet_eta = 0;
   truth_jet_flavor = 0;
   truth_jet_isIsolated = 0;
   truth_jet_m = 0;
   truth_jet_phi = 0;
   truth_jet_pt = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("HLT_jet_Et", &HLT_jet_Et, &b_HLT_jet_Et);
   fChain->SetBranchAddress("HLT_jet_eta", &HLT_jet_eta, &b_HLT_jet_eta);
   fChain->SetBranchAddress("HLT_jet_phi", &HLT_jet_phi, &b_HLT_jet_phi);
   fChain->SetBranchAddress("L1_jet_E", &L1_jet_E, &b_L1_jet_E);
   fChain->SetBranchAddress("L1_jet_eta", &L1_jet_eta, &b_L1_jet_eta);
   fChain->SetBranchAddress("L1_jet_phi", &L1_jet_phi, &b_L1_jet_phi);
   fChain->SetBranchAddress("MC_weight", &MC_weight, &b_MC_weight);
   fChain->SetBranchAddress("event_Centrality", &event_Centrality, &b_event_Centrality);
   fChain->SetBranchAddress("event_EdgeGapNeg", &event_EdgeGapNeg, &b_event_EdgeGapNeg);
   fChain->SetBranchAddress("event_EdgeGapPos", &event_EdgeGapPos, &b_event_EdgeGapPos);
   fChain->SetBranchAddress("event_Et", &event_Et, &b_event_Et);
   fChain->SetBranchAddress("event_EventNumber", &event_EventNumber, &b_event_EventNumber);
   fChain->SetBranchAddress("event_FCal_Et", &event_FCal_Et, &b_event_FCal_Et);
   fChain->SetBranchAddress("event_FCal_Et_A", &event_FCal_Et_A, &b_event_FCal_Et_A);
   fChain->SetBranchAddress("event_FCal_Et_C", &event_FCal_Et_C, &b_event_FCal_Et_C);
   fChain->SetBranchAddress("event_FCal_Et_N", &event_FCal_Et_N, &b_event_FCal_Et_N);
   fChain->SetBranchAddress("event_FCal_Et_P", &event_FCal_Et_P, &b_event_FCal_Et_P);
   fChain->SetBranchAddress("event_RunNumber", &event_RunNumber, &b_event_RunNumber);
   fChain->SetBranchAddress("event_Status_ZDC_A", &event_Status_ZDC_A, &b_event_Status_ZDC_A);
   fChain->SetBranchAddress("event_Status_ZDC_C", &event_Status_ZDC_C, &b_event_Status_ZDC_C);
   fChain->SetBranchAddress("event_SumGapsNeg", &event_SumGapsNeg, &b_event_SumGapsNeg);
   fChain->SetBranchAddress("event_SumGapsPos", &event_SumGapsPos, &b_event_SumGapsPos);
   fChain->SetBranchAddress("event_ZDC_A", &event_ZDC_A, &b_event_ZDC_A);
   fChain->SetBranchAddress("event_ZDC_C", &event_ZDC_C, &b_event_ZDC_C);
   fChain->SetBranchAddress("event_ZDC_modules", &event_ZDC_modules, &b_event_ZDC_modules);
   fChain->SetBranchAddress("event_ZDC_modules_status", &event_ZDC_modules_status, &b_event_ZDC_modules_status);
   fChain->SetBranchAddress("event_lbn", &event_lbn, &b_event_lbn);
   fChain->SetBranchAddress("event_mu", &event_mu, &b_event_mu);
   fChain->SetBranchAddress("event_nTrk", &event_nTrk, &b_event_nTrk);
   fChain->SetBranchAddress("event_npv", &event_npv, &b_event_npv);
   fChain->SetBranchAddress("event_q_N_Cos", &event_q_N_Cos, &b_event_q_N_Cos);
   fChain->SetBranchAddress("event_q_N_Sin", &event_q_N_Sin, &b_event_q_N_Sin);
   fChain->SetBranchAddress("event_q_P_Cos", &event_q_P_Cos, &b_event_q_P_Cos);
   fChain->SetBranchAddress("event_q_P_Sin", &event_q_P_Sin, &b_event_q_P_Sin);
   fChain->SetBranchAddress("event_v_pv_z", &event_v_pv_z, &b_event_v_pv_z);
   fChain->SetBranchAddress("event_v_vtx_tracks", &event_v_vtx_tracks, &b_event_v_vtx_tracks);
   fChain->SetBranchAddress("jet_E", &jet_E, &b_jet_E);
   fChain->SetBranchAddress("jet_E_unsubtracted", &jet_E_unsubtracted, &b_jet_E_unsubtracted);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_eta_emscale", &jet_eta_emscale, &b_jet_eta_emscale);
   fChain->SetBranchAddress("jet_eta_unsubtracted", &jet_eta_unsubtracted, &b_jet_eta_unsubtracted);
   fChain->SetBranchAddress("jet_isGood", &jet_isGood, &b_jet_isGood);
   fChain->SetBranchAddress("jet_m", &jet_m, &b_jet_m);
   fChain->SetBranchAddress("jet_n", &jet_n, &b_jet_n);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_pt_emscale", &jet_pt_emscale, &b_jet_pt_emscale);
   fChain->SetBranchAddress("jet_pt_unsubtracted", &jet_pt_unsubtracted, &b_jet_pt_unsubtracted);
   fChain->SetBranchAddress("jet_rtrk", &jet_rtrk, &b_jet_rtrk);
   fChain->SetBranchAddress("jet_timing", &jet_timing, &b_jet_timing);
   fChain->SetBranchAddress("trigger_decisions", &trigger_decisions, &b_trigger_decisions);
   fChain->SetBranchAddress("trigger_decisions_emulated", &trigger_decisions_emulated, &b_trigger_decisions_emulated);
   fChain->SetBranchAddress("triggers_HLT_passedRAW", &triggers_HLT_passedRAW, &b_triggers_HLT_passedRAW);
   fChain->SetBranchAddress("triggers_HLT_prescaled", &triggers_HLT_prescaled, &b_triggers_HLT_prescaled);
   fChain->SetBranchAddress("triggers_HLT_rerun", &triggers_HLT_rerun, &b_triggers_HLT_rerun);
   fChain->SetBranchAddress("triggers_L1_passedAfterPrescale", &triggers_L1_passedAfterPrescale, &b_triggers_L1_passedAfterPrescale);
   fChain->SetBranchAddress("triggers_L1_passedAfterVeto", &triggers_L1_passedAfterVeto, &b_triggers_L1_passedAfterVeto);
   fChain->SetBranchAddress("triggers_L1_passedBeforePrescale", &triggers_L1_passedBeforePrescale, &b_triggers_L1_passedBeforePrescale);
   fChain->SetBranchAddress("triggers_prescale", &triggers_prescale, &b_triggers_prescale);
   fChain->SetBranchAddress("truth_jet_E", &truth_jet_E, &b_truth_jet_E);
   fChain->SetBranchAddress("truth_jet_eta", &truth_jet_eta, &b_truth_jet_eta);
   fChain->SetBranchAddress("truth_jet_flavor", &truth_jet_flavor, &b_truth_jet_flavor);
   fChain->SetBranchAddress("truth_jet_isIsolated", &truth_jet_isIsolated, &b_truth_jet_isIsolated);
   fChain->SetBranchAddress("truth_jet_m", &truth_jet_m, &b_truth_jet_m);
   fChain->SetBranchAddress("truth_jet_n", &truth_jet_n, &b_truth_jet_n);
   fChain->SetBranchAddress("truth_jet_phi", &truth_jet_phi, &b_truth_jet_phi);
   fChain->SetBranchAddress("truth_jet_pt", &truth_jet_pt, &b_truth_jet_pt);
   Notify();
}

Bool_t AntiKt4HI::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void AntiKt4HI::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t AntiKt4HI::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef AntiKt4HI_cxx
