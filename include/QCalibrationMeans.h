#ifndef QCALIBRATIONMEANS_H
#define QCALIBRATIONMEANS_H

#include "TH1F.h"
#include "vector"

class QCalibrationMeans : public TObject
{
public:
  /**
   * @brief Construct a new Data object
   *
   * @param
   */

  QCalibrationMeans(UInt_t RunN=0, int NumCentBins=0, int NumHarmonics=0);
  QCalibrationMeans(const QCalibrationMeans &q)
  {
    m_runNumber = q.m_runNumber;
    m_numCentBins = q.m_numCentBins;
    m_numHarmonics = q.m_numHarmonics;
    TH1::AddDirectory(kFALSE);
    for (auto rawQvec : q.m_rawQvecHistsReP){
      m_rawQvecHistsReP.push_back(static_cast<TH1F *>(rawQvec->Clone()));
    }
    for (auto rawQvec : q.m_rawQvecHistsReN)
    {
      m_rawQvecHistsReN.push_back(static_cast<TH1F *>(rawQvec->Clone()));
    }
    for (auto rawQvec : q.m_rawQvecHistsImP)
    {
      m_rawQvecHistsImP.push_back(static_cast<TH1F *>(rawQvec->Clone()));
    }
    for (auto rawQvec : q.m_rawQvecHistsImN)
    {
      m_rawQvecHistsImN.push_back(static_cast<TH1F *>(rawQvec->Clone()));
    }
    std::copy(q.m_AvgQ_Re.begin(), q.m_AvgQ_Re.end(), back_inserter(m_AvgQ_Re));
    std::copy(q.m_AvgQ_Im.begin(), q.m_AvgQ_Im.end(), back_inserter(m_AvgQ_Im));
  }
  /**
   * @brief Tree variables
   */
  void AddElements(std::vector<float> *qx, std::vector<float> *qy, int cent, float FCal_Et_P, float FCal_Et_N);
  double AvgQnP_Re(int n, int cent) {
    return m_AvgQ_Re.at(cent).at(n);
  }
  double AvgQnP_Im(int n, int cent) {
    return m_AvgQ_Im.at(cent).at(n);
  }
  double AvgQnN_Re(int n, int cent) {
    return m_AvgQ_Re.at(cent).at(n+m_numHarmonics);
  }
  double AvgQnN_Im(int n, int cent) {
    return m_AvgQ_Im.at(cent).at(n+m_numHarmonics);
  }
  TH1F* QnP_Re(int n, int cent) { return m_rawQvecHistsReP.at(cent + m_numCentBins * n); }
  TH1F* QnP_Im(int n, int cent) { return m_rawQvecHistsImP.at(cent + m_numCentBins * n); }
  TH1F* QnN_Re(int n, int cent) { return m_rawQvecHistsReN.at(cent + m_numCentBins * n); }
  TH1F* QnN_Im(int n, int cent) { return m_rawQvecHistsImN.at(cent + m_numCentBins * n); }
  void FinalizeElements();
  std::vector<float> InClassAvgQ_Re(int cent);
  std::vector<float> InClassAvgQ_Im(int cent);
  std::vector<float> *AvgQ_Re(int cent){ return &m_AvgQ_Re.at(cent); }
  std::vector<float> *AvgQ_Im(int cent) { return &m_AvgQ_Im.at(cent); }
  UInt_t getRunNumber() {return m_runNumber;}

protected:
  /**
   * @brief pointer to the TTree (or TChain) class
   */
  UInt_t m_runNumber;
  int m_numCentBins;
  int m_numHarmonics;
  std::vector<TH1F*> m_rawQvecHistsReP;
  std::vector<TH1F*> m_rawQvecHistsImP;
  std::vector<TH1F*> m_rawQvecHistsReN;
  std::vector<TH1F*> m_rawQvecHistsImN;
  std::vector<std::vector<float>> m_AvgQ_Re;  //outside is centrality, inside is harmonics
  std::vector<std::vector<float>> m_AvgQ_Im;

  ClassDef(QCalibrationMeans, 1);
};

#endif
