import sys
sys.path.insert(0, '/Users/mhoppesch/jetsResarch/pPbAnalysisHistograms/python')
# here we load the shared library created using the Makefile
from ROOT import gSystem, TH1, TStopwatch
gSystem.Load("Analysis.so")
TH1.AddDirectory(False)

# now we can create instance of the class EventLoop
from python.Samples import *
#eventLoop = SampleGridTestv3()
eventLoops = []
eventLoops += [SampleMinBias()]
eventLoops += [SampleJetTriggers()]
eventLoops += [SampleGridTestv3()]
# create algorithm
from python.Algorithms import *
for eventLoop in eventLoops:
  algs = []
  algs += [ AlgDF() ]
  # add the algorithm into the event loop
  eventLoop.addAlgorithms( algs )
# execute parallel jobs
timer = TStopwatch()
from python.Jobs import Jobs
jobs = Jobs(eventLoops)
jobs.execute()
timer.Stop()
print ("The processing took {} s".format(timer.RealTime()))

