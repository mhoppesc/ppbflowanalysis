import sys
sys.path.insert(0, '/Users/mhoppesch/jetsResarch/pPbAnalysisHistograms/python')
from Plot import Plot
from ROOT import TCanvas, TLegend, kRed, kGreen, kBlack, TMultiGraph

# set ATLAS plot style
from AtlasStyle import setStyle
setStyle()
kGreen = 416
kRed = 632
kBlue = 600
kMagenta = 616
kCyan = 432
# draw plots
histNames = ['v2_c0_SP', 'v3_c0_SP','v4_c0_SP','v2_c2_SP', 'v3_c2_SP','v4_c2_SP']
sampleNames = ['PbPb2018AllVertex']
algNames = ['roughPtMatch']
canvases = []
plots = []
legends = []
color_list = [kGreen,kRed, kBlue, kMagenta+2, kCyan+3]
transperency = [.5,.5,.5,.5,.8]
flow  = 2
for histName in histNames:
 

 # load individual plots
 plots_MinBias = []
 plots_JetTriggers = []

 for algName in algNames:
  plots_MinBias += [ Plot("histograms/histograms.MinBias.{}.root".format(algName), histName) ]
  plots_JetTriggers += [ Plot("histograms/histograms.JetTriggers.{}.root".format(algName), histName) ] 

 # add plots from two samples into a single plot
 plot_MinBias = Plot(plots_MinBias)
 plot_JetTriggers = Plot(plots_JetTriggers)

 # set style
 plot_MinBias.setStyleMarker(kRed)
 plot_JetTriggers.setStyleMarker(kGreen)

 # create a THStack
 #plot = Plot([plot_MinBias, plots_JetTriggers], True)
 
 plot = TMultiGraph(); 
 plot.Add(plot_MinBias.hist2Graph(kRed))
 plot.Add(plot_JetTriggers.hist2Graph(kBlue))
 
 # create a canvas
 c = TCanvas(histName, histName, 800, 600)
 
 
 
 gPad = c.cd(1)
 gPad.SetLogx()
 # draw
 plot.Draw('AP')
 plot.GetYaxis().SetTitle("v{}".format(flow))
 plot.GetXaxis().SetTitle("pT [GeV]")
 # Create the same plot, but this time merging the histograms
 # We will set to the "errorbar style" and overlay the TH stack
 #plotErr = Plot([plot_MinBias, plots_JetTriggers])
 #plotErr.setStyleErrorbar(kBlack)
 #plotErr.draw("same")

 # draw legend
 """  legend = TLegend(0.6, 0.9, 0.9, 0.65)
 legend.AddEntry(plot_MinBias, "MinBias", "f")
 legend.AddEntry(plot_JetTriggers, "JetTriggers", "f")
 legend.Draw("same") """

 # save the canvas
 c.Print("pdfs/{}.pdf".format(histName))

 # save the instances so they are not deleted by the garbage collector
 canvases += [ c ]
 plots += [ plot ]
 flow += 1
 #legends += [ legend ]
