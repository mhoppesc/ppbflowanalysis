from ROOT import CalibrationLoop as CalibrationLoopCpp


class CalibrationLoop(object):
 """ Wrapper around the c++ class Calibration loop
 """

 def __init__(self, name):
  # set the Calibration loop name
  self.name = name

  # now we can create instance of the c++ class CalibrationLoop
  self.calibrationLoop = CalibrationLoopCpp()

  # set the tree name attribute
  self.calibrationLoop.treeName = "AntiKt4HI"
  
  # set the outputFileName
  self.calibrationLoop.calibFileName = "QVecAndTrackCalibFiles/{}_calib.root".format(name)

   
 def execute(self):
  """ initialize and execute the Calibration loop
  """
  self.calibrationLoop.initialize()
  self.calibrationLoop.execute()
