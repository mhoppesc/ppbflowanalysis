from Algorithm import Algorithm
from ROOT import TH1D
from ROOT import TH2D
import array

# -----------------------------------


class AlgDF(Algorithm):
 """ Default set of histograms
 """

 def __init__(self, name="roughPtMatchSP"):
  # call inherited constructor
  Algorithm.__init__(self, name, ['h_flowSP',
                                  'h_flowNum',
                                  'h_rawHarmonicsNum',
                                  'h_rawResolutions',
                                  'h_rawQvecsRe',
                                  'h_rawQvecsIm',
                                  'h_calibQvecsRe',
                                  'h_calibQvecsIm',
                                  'h_rawHarmonics',
                                  'h_meanCalibQvectorHistReal',
                                  'h_meanCalibQvectorHistImaginary',
                                  'h_meanRawQvectorHistReal',
                                  'h_meanRawQvectorHistImaginary',
                                  'h_trackWeights'])
  ptMin = 1
  num_harmonics = 5
  centralityMin = -.5
  centralityMax = 9.5
  nBinsCentrality = 10
  edges = array.array('d',[.10, .250, .50, .80,
              1.00, 1.250, 1.50, 2.00, 
              2.50, 3.00, 3.50, 4.00, 
              5.00, 6.00, 7.00, 8.00, 
              10.00, 12.00, 20.00, 40.00, 
              120.00, 350.00])
  """   edges  = array.array('d',[0.5, 0.75, 1.0, 1.25, 
              1.5, 2.0, 2.5, 3.0, 
              3.5, 4.0, 5.0, 6.0, 
              7.0, 8.0, 10.0,12.0, 
              14.0, 20.0, 26.0, 35.0]) """
  # create histograms
  #self.alg.h_v2_c0 = TH1D("v2_c0", ";pT;v2",len(edges) - 1, edges)
  for centrality_counter in range(4):
    for harmonic_counter in range(5):
      self.alg.h_flowSP[harmonic_counter + num_harmonics*centrality_counter] = TH1D("v{n}_c{c}_SP".format(n=harmonic_counter+2,c=centrality_counter), ";pT;v{n}".format(n=harmonic_counter+2),len(edges) - 1, edges)
      self.alg.h_flowNum[harmonic_counter + num_harmonics*centrality_counter] = TH1D("Num_v{n}_c{c}_SP".format(n=harmonic_counter+2,c=centrality_counter), ";pT;Num_v{n}".format(n=harmonic_counter+2),len(edges) - 1, edges)
      
  for harmonic_counter in range(5):
    self.alg.h_rawQvecsRe[harmonic_counter] = TH1D("Q{n}_Re_Raw".format(n=harmonic_counter+2), ";Centrality;Re#langle Q{n} #rangle_Raw".format(n=harmonic_counter+2),nBinsCentrality,centralityMin,centralityMax)
    self.alg.h_rawQvecsIm[harmonic_counter] = TH1D("Q{n}_Im_Raw".format(n=harmonic_counter+2), ";Centrality;Im#langle Q{n} #rangle_Raw".format(n=harmonic_counter+2),nBinsCentrality,centralityMin,centralityMax)
    self.alg.h_calibQvecsRe[harmonic_counter] = TH1D("Q{n}_Re_Calib".format(n=harmonic_counter+2), ";Centrality;Re#langle Q{n} #rangle_Calib".format(n=harmonic_counter+2),nBinsCentrality,centralityMin,centralityMax)
    self.alg.h_calibQvecsIm[harmonic_counter] = TH1D("Q{n}_Im_Calib".format(n=harmonic_counter+2), ";Centrality;Im#langle Q{n} #rangle_Calib".format(n=harmonic_counter+2),nBinsCentrality,centralityMin,centralityMax)
  #= TH1D("v{n}_c0".format(n=i+2), ";pT;v{n}".format(n=i+2),len(edges) - 1, edges)
  #print(type(self.alg.h_v2_c0))
# -----------------------------------

