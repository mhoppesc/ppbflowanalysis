from CalibrationLoop import CalibrationLoop
import os
# -----------------------------------


class SampleSmall(CalibrationLoop):
 """ event loop over the gluon-gluon fusion sample
 """

 def __init__(self):
  # call the inherited constructor
  CalibrationLoop.__init__(self, "smallSample")

  # add the ggH samples into the event loop
  #self.calibrationLoop.inputFiles.push_back('../pPb_nTuples/user.mhoppesc.pPp_2016_Tracks_noJets_6_12_2023_MinBias_TeV_ANALYSIS/user.mhoppesc.00313187.r11416_p3868.33726019.ANALYSIS._000001.root')
  self.calibrationLoop.inputFiles.push_back('../../../../../../../eos/user/m/mhoppesc/datasets/data16_pPb/user.mhoppesc.pPp_2016_Tracks_noJets_6_12_2023_MinBias_TeV_ANALYSIS/user.mhoppesc.00313063.r11416_p3868.33726019.ANALYSIS._000014.root')
  self.calibrationLoop.inputFiles.push_back('../../../../../../../eos/user/m/mhoppesc/datasets/data16_pPb/user.mhoppesc.pPp_2016_Tracks_noJets_6_12_2023_MinBias_TeV_ANALYSIS/user.mhoppesc.00313187.r11416_p3868.33726019.ANALYSIS._000001.root')

class SampleGridTestv3(CalibrationLoop):
 """ event loop over the gluon-gluon fusion sample
 """

 def __init__(self):
  # call the inherited constructor
  CalibrationLoop.__init__(self, "GridTestSamplev3")
  self.eosFilePath = '/eos/user/m/mhoppesc/datasets/data16_pPb/user.mhoppesc.pPp_2016_moreTracks_noJets_6_8_2023_ANALYSIS'
  # add the samples into the event loop
  for filename in os.listdir(self.eosFilePath):
    self.calibrationLoop.inputFiles.push_back('{filePath}/{file}'.format(filePath = self.eosFilePath,file = filename))

# -----------------------------------


class SampleJetTriggers(CalibrationLoop):
 """ event loop over the gluon-gluon fusion sample
 """

 def __init__(self):
  # call the inherited constructor
  CalibrationLoop.__init__(self, "JetTriggers")
  self.eosFilePath = '/eos/user/m/mhoppesc/datasets/data16_pPb/user.mhoppesc.pPp_2016_Tracks_noJets_6_12_2023_JetTriggers_TeV_ANALYSIS'
  # add the samples into the event loop
  for filename in os.listdir(self.eosFilePath):
    self.calibrationLoop.inputFiles.push_back('{filePath}/{file}'.format(filePath = self.eosFilePath,file = filename))

# -----------------------------------


class SampleMinBias(CalibrationLoop):
 """ event loop over the gluon-gluon fusion sample
 """

 def __init__(self):
  # call the inherited constructor
  CalibrationLoop.__init__(self, "MinBias")

  # add the samples into the event loop
  self.eosFilePath = '/eos/user/m/mhoppesc/datasets/data16_pPb/user.mhoppesc.pPp_2016_Tracks_noJets_6_12_2023_MinBias_TeV_ANALYSIS'
  # add the samples into the event loop
  for filename in os.listdir(self.eosFilePath):
    self.calibrationLoop.inputFiles.push_back('{filePath}/{file}'.format(filePath = self.eosFilePath,file = filename))
