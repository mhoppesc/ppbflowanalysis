#include "../include/TrackCalibrationWeights.h"

TrackCalibrationWeights::TrackCalibrationWeights(UInt_t RunN){
  m_runNumber=RunN;
  m_numEventsRunCounter = 0;
  m_numBinsTrkWgt = {64,51};
  m_limitBinsTrkWgt = {3.14,2.5};
  m_trackWeight2D = new TH2D(Form("TrackWeight2d_%d",m_runNumber), "Track Weights Raw;#phi;#eta", m_numBinsTrkWgt.first, -1*m_limitBinsTrkWgt.first, m_limitBinsTrkWgt.first + .1, m_numBinsTrkWgt.second, -1*m_limitBinsTrkWgt.second,m_limitBinsTrkWgt.second+.1);
  m_trackWeight1D = new TH1D(Form("TrackWeight1d_%d",m_runNumber), "Track Weights Raw:#eta;", m_numBinsTrkWgt.second, -1*m_limitBinsTrkWgt.second,m_limitBinsTrkWgt.second);
  m_trackWeightFinal = new TH2D(Form("TrackWeight_%d",m_runNumber), "Track Weights Raw;#phi;#eta", m_numBinsTrkWgt.first, -1*m_limitBinsTrkWgt.first, m_limitBinsTrkWgt.first +.1, m_numBinsTrkWgt.second, -1*m_limitBinsTrkWgt.second,m_limitBinsTrkWgt.second+.1);
  m_trackMapCorrected = new TH2D(Form("TrackMapFinal_%d", m_runNumber), "Tracks Corrected;#phi;#eta", m_numBinsTrkWgt.first, -1 * m_limitBinsTrkWgt.first, m_limitBinsTrkWgt.first+.1, m_numBinsTrkWgt.second, -1 * m_limitBinsTrkWgt.second, m_limitBinsTrkWgt.second+.1);
}

void TrackCalibrationWeights::AddElements(
                                        std::vector<float> *trk_eta,
                                        std::vector<float> *trk_phi)
{
  m_numEventsRunCounter += 1;
  for(int i=0; i<trk_eta->size(); i++){
  m_trackWeight2D->Fill(trk_phi->at(i),trk_eta->at(i));
  m_trackWeight1D->Fill(trk_eta->at(i));
  }
}

void TrackCalibrationWeights::FinalizeElements(){
  //try without averaging 
  //m_trackWeight1D->Scale(1. / static_cast<double> (m_numEventsRunCounter));
  ///m_trackWeight2D->Scale(1. /static_cast<double>( m_numEventsRunCounter ));
  for(int i=0; i < m_numBinsTrkWgt.first; i++){
    for(int j=0; j < m_numBinsTrkWgt.second; j++){
      m_trackWeightFinal->SetBinContent(i,j,m_trackWeight1D->GetBinContent(j) / m_trackWeight2D->GetBinContent(i,j));
      m_trackMapCorrected->SetBinContent(i, j, m_trackWeight1D->GetBinContent(j));
      }
  }
}
