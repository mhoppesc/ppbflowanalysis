#include "../include/Algorithm.h"

Algorithm::Algorithm()
{
}

void Algorithm::initialize(Data *data, std::vector<Harmonic *> *harmonics, std::vector<MeanQvector *> *meanQvectors, std::vector<TrackCalibrationWeights*> *trackCalib, std::vector<Resolution *> *resolutions)
{
  m_data = data;
  m_harmonics = harmonics;
  m_resolutions = resolutions;
  m_meanQvectors = meanQvectors;
  m_trackCalib = trackCalib;
  m_num_harmonics = 5;
  m_num_VnCentBins = 4;
}
void Algorithm::executeData()
{


}
void Algorithm::executeHarmonic()
{
  for(auto harmonic: *m_harmonics){
  // apply selection. Exit if didn't pass
  //if (!passedSelectionHarmonic(harmonic->getN()-3,harmonic->getcentBin()))
    //continue;
  //if (harmonic->getptBin() == 0 || harmonic->getptBin() == 1 || harmonic->getptBin() == 2 || harmonic->getptBin() == 3 || harmonic->getptBin() == 4 || harmonic->getptBin() == 10 || harmonic->getptBin() == 30){
    //h_rawHarmonicsEP.push_back(harmonic->getHistEP());
  h_rawHarmonicsNum.push_back(harmonic->getHist());
  
  harmonic->buffEmpty();
  harmonic->resetRange();
  std::pair<double, double> VnPair = computeVn(harmonic->getNumVn(),
                                               harmonic->getNumVnErr(),
                                               m_resolutions->at(harmonic->getcentBin() + m_num_VnCentBins * (harmonic->getN() - 2))->getRes(),
                                               m_resolutions->at(harmonic->getcentBin() + m_num_VnCentBins * (harmonic->getN() - 2))->getResErr());
  // fill the plots
  fillPlotsHarmonic(harmonic->getN()-2, harmonic->getcentBin(), harmonic->getptBin()+1, VnPair.first, VnPair.second,harmonic->getNumVn(),
                                               harmonic->getNumVnErr() );
}
}

bool Algorithm::passedSelectionHarmonic(int n, int centBin)
{
  // select specific flavor combinations
  if(n != 0)
    return false;
  if(centBin != 0)
    return false;
  // passed all the cuts
  return true;
}

void Algorithm::fillPlotsHarmonic(int n, int centBin, int bin, double meanSP, double errSP, double meanNum, double meanNumErr)
{
  // here we fill the histograms. We protect the code against the empty pointers.
  /* if(h_v2_c0){
    h_v2_c0->SetBinContent(bin, mean);
    h_v2_c0->SetBinError(bin, error);
  } */

  h_flowSP.at(centBin*m_num_harmonics+n).SetBinContent(bin, meanSP);
  h_flowSP.at(centBin * m_num_harmonics + n).SetBinError(bin, errSP);
  h_flowNum.at(centBin * m_num_harmonics + n).SetBinContent(bin, meanNum);
  h_flowNum.at(centBin * m_num_harmonics + n).SetBinError(bin, meanNum);
}

void Algorithm::executeMeanQvectors()
{
  for (auto Qvec : *m_meanQvectors)
  {
    
    h_meanCalibQvectorHistReal.push_back(Qvec->get_meanCalibQvectorHistReal());
    h_meanCalibQvectorHistImaginary.push_back( Qvec->get_meanCalibQvectorHistImaginary());
    h_meanRawQvectorHistReal.push_back(Qvec->get_meanRawQvectorHistReal());
    h_meanRawQvectorHistImaginary.push_back(Qvec->get_meanRawQvectorHistImaginary());

    // fill the plots
    fillPlotsMeanQVectors(Qvec->getN()-2, Qvec->getIsN(), Qvec->getcentBin()+1, Qvec->getCalibMeanQRe(), Qvec->getCalibMeanQReErr(),
                                                                            Qvec->getCalibMeanQIm(), Qvec->getCalibMeanQImErr(), Qvec->getRawMeanQRe(),Qvec->getRawMeanQIm());
  }
}

void Algorithm::fillPlotsMeanQVectors(int n,bool isN,int bin,double meanCalibRe, double errorCalibRe,
                                  double meanCalibIm, double errorCalibIm,
                                  double meanRawRe, double meanRawIm)
{
  if(!isN){
    h_calibQvecsRe.at(n).SetBinContent(bin, meanCalibRe);
    h_calibQvecsIm.at(n).SetBinContent(bin, meanCalibIm);
    h_rawQvecsRe.at(n).SetBinContent(bin, meanRawRe);
    h_rawQvecsIm.at(n).SetBinContent(bin, meanRawIm);
  }
}

void Algorithm::executeTrackCalib()
{
   for (auto calib : *m_trackCalib)
  {
    h_trackWeights.push_back(calib->trackWeight());
    h_trackWeights.push_back(calib->trackMapInit());
    h_trackWeights.push_back(calib->trackMapCorrected());
  }
}

std::pair<double, double> Algorithm::computeVn(double Numerator, double NumeratorErr, double Res, double ResErr) // Denomiator of Vn calculation is reffered to as the resolution
{
  // propogation of errors
  if(Res < 0 )
    return std::make_pair(0,0);
  double Denominator = TMath::Sqrt(Res);
  double DenominatorErr = Denominator * TMath::Sqrt(TMath::Power(0.5*ResErr / Res, 2));
  double Vn = Numerator / Denominator;
  double VnErr = Vn * TMath::Sqrt(TMath::Power(NumeratorErr / Numerator, 2) + TMath::Power(DenominatorErr / Denominator, 2));
  return std::make_pair(Vn, VnErr);
}

void Algorithm::executeResolutions(){
  
  for(auto res : * m_resolutions)
    h_rawResolutions.push_back(res->getHistRes());
}
