#include "../include/MeanQvector.h"

MeanQvector::MeanQvector(int n, int centralityBin, bool isN){
  m_meanCalibQvectorHistReal = new TH1F(Form("ReQ%d_CentralityClass_%d_Side_%d_Calib", n, centralityBin, !isN),
                                        Form(";#langle Q%d #rangle.Re_{Calib%d};counts", n, centralityBin), 100, 0, 0);
  m_meanCalibQvectorHistImaginary = new TH1F(Form("ImQ%d_CentralityClass_%d_Side_%d_Calib", n, centralityBin, !isN),
                                             Form(";#langle Q%d #rangle.Im_{Calib_%d};counts", n, centralityBin), 100, 0, 0);
  m_meanRawQvectorHistReal = new TH1F(Form("ReQ%d_CentralityClass_%d_Side_%d_Raw", n, centralityBin, !isN),
                                      Form(";#langle Q%d #rangle.Re_{Raw%d};counts", n, centralityBin), 100, 0, 0);
  m_meanRawQvectorHistImaginary = new TH1F(Form("ImQ%d_CentralityClass_%d_Side_%d_Raw", n, centralityBin, !isN),
                                           Form(";#langle Q%d #rangle.Im_{Raw_%d};counts", n, centralityBin), 100, 0, 0);

  m_n = n;
  m_centralityBin = centralityBin;
  m_isN = isN;
}

void MeanQvector::AddToAverage(TComplex rawQ, TComplex calibQ){
  TH1::StatOverflows(kTRUE);
  m_meanCalibQvectorHistReal->Fill(calibQ.Re());
  m_meanCalibQvectorHistImaginary->Fill(calibQ.Im());
  m_meanRawQvectorHistReal->Fill(rawQ.Re());
  m_meanRawQvectorHistImaginary->Fill(rawQ.Im());
}
