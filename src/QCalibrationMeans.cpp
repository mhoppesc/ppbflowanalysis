#include "../include/QCalibrationMeans.h"

QCalibrationMeans::QCalibrationMeans(UInt_t RunN, int NumCentBins, int NumHarmonics)
{
  m_runNumber = RunN;
  m_numCentBins = NumCentBins;
  m_numHarmonics = NumHarmonics;
  m_rawQvecHistsReP = std::vector<TH1F *>(NumCentBins * NumHarmonics);
  m_rawQvecHistsImP = std::vector<TH1F *>(NumCentBins * NumHarmonics);
  m_rawQvecHistsReN = std::vector<TH1F *>(NumCentBins * NumHarmonics);
  m_rawQvecHistsImN = std::vector<TH1F *>(NumCentBins * NumHarmonics);
  for(int i=0;i<NumHarmonics;i++){
  for(int j=0;j<NumCentBins;j++)
  {
    m_rawQvecHistsReP.at(j + i * NumCentBins) = new TH1F(Form("ReAvgQ%d_CentralityClass_%d_Run_%d_Side_P", i + 2, j,RunN),
                                                       Form(";ReAvgQ%d_CentralityClass_%d_Run_%d;counts", i+2, j,RunN), 100, 0, 0);
    m_rawQvecHistsReN.at(j + i * NumCentBins) = new TH1F(Form("ReAvgQ%d_CentralityClass_%d_Run_%d_Side_N", i + 2, j, RunN),
                                                        Form(";ReAvgQ%d_CentralityClass_%d_Run_%d;counts", i + 2, j, RunN), 100, 0, 0);
    m_rawQvecHistsImP.at(j + i * NumCentBins) = new TH1F(Form("ImAvgQ%d_CentralityClass_%d_Run_%d_Side_P", i + 2, j,RunN),
                                                        Form(";ImAvgQ%d_CentralityClass_%d_Run_%d;counts", i + 2, j,RunN), 100, 0, 0);
    m_rawQvecHistsImN.at(j + i * NumCentBins) = new TH1F(Form("ImAvgQ%d_CentralityClass_%d_Run_%d_Side_N", i + 2, j, RunN),
                                                        Form(";ImAvgQ%d_CentralityClass_%d_Run_%d;counts", i + 2, j, RunN), 100, 0, 0);
  }
  }
}
void QCalibrationMeans::AddElements(std::vector<float> *qx, std::vector<float> *qy, int cent, float FCal_Et_P, float FCal_Et_N)
{
  TH1::StatOverflows(kTRUE);

  for (int i = 0; i < m_numHarmonics; i++)
  {// take out conversion factor
  m_rawQvecHistsReP.at(cent + m_numCentBins * i)->Fill(qx->at(i) / FCal_Et_P);
  m_rawQvecHistsImP.at(cent + m_numCentBins * i)->Fill(qy->at(i) / FCal_Et_P);
  m_rawQvecHistsReN.at(cent + m_numCentBins * i)->Fill(qx->at(i + m_numHarmonics) / FCal_Et_N );
  m_rawQvecHistsImN.at(cent + m_numCentBins * i)->Fill(qy->at(i + m_numHarmonics) / FCal_Et_N) ;
  }
}

void QCalibrationMeans::FinalizeElements(){
  for(int i =0;i<m_numCentBins;i++){
  m_AvgQ_Re.push_back(QCalibrationMeans::InClassAvgQ_Re(i));
  m_AvgQ_Im.push_back(QCalibrationMeans::InClassAvgQ_Im(i));
  }
}

  std::vector<float> QCalibrationMeans::InClassAvgQ_Re(int cent)
  {
  std::vector<float> temp(m_numHarmonics*2);
  
  for (int i = 0; i < m_numHarmonics; i++)
  {
    temp.at(i) = m_rawQvecHistsReP.at(cent + m_numCentBins * i)->GetMean();
    temp.at(i + m_numHarmonics) = m_rawQvecHistsReN.at(cent + m_numCentBins * i)->GetMean();
  }
  return temp;
  }

  std::vector<float> QCalibrationMeans::InClassAvgQ_Im(int cent)
  {
  std::vector<float> temp(m_numHarmonics*2);
  for (int i = 0; i < m_numHarmonics; i++)
  {
    temp.at(i) = m_rawQvecHistsImP.at(cent + m_numCentBins * i)->GetMean();
    temp.at(i + m_numHarmonics) = m_rawQvecHistsImN.at(cent + m_numCentBins * i)->GetMean();
  }
  return temp;
  }

