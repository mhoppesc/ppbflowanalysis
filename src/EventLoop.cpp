#include "../include/EventLoop.h"
#include <iostream>
#include <stdio.h>
#include <stdexcept>
#include <bitset>
#include <chrono>
#include "TFile.h"

ClassImp(TrackCalibrationWeights);
ClassImp(QCalibrationMeans) // add qcalibmeans and map

    void EventLoop::initialize()
{
  auto start = std::chrono::high_resolution_clock::now();
  // create an instance of the TChain class
  m_chain = new TChain(treeName);

  // loop through the input files and add them to the chain
  for (auto inputFile : inputFiles)
  {
    m_chain->Add(inputFile);
    std::cout << "Added file: " << inputFile << std::endl;
  }
  //cache size
  m_chain->SetCacheSize(-1);
  
  // init pt binning
/*   pt_range = {0.5e-3, 0.75e-3, 1.0e-3, 1.25e-3,
              1.5e-3, 2.0e-3, 2.5e-3, 3.0e-3,
              3.5e-3, 4.0e-3, 5.0e-3, 6.0e-3,
              7.0e-3, 8.0e-3, 10.0e-3, 12.0e-3,
              14.0e-3, 20.0e-3, 26.0e-3, 35.0e-3}; */
  pt_range = {.1e-3, .25e-3, .5e-3, .8e-3,
              1.0e-3, 1.25e-3, 1.5e-3, 2.0e-3, 
              2.5e-3, 3.0e-3, 3.5e-3, 4.0e-3, 
              5.0e-3, 6.0e-3, 7.0e-3, 8.0e-3, 
              10.0e-3, 12.0e-3, 20.0e-3, 40.0e-3, 
              120.0e-3, 350.0e-3};

  for (int i = 0; i < 63; i++)
  {
    phi_range.push_back(-1 * 3.14 + (6.28 / 63) * i);
  }
  for (int i = 0; i < 50; i++)
  {
    eta_range.push_back(-1 * 2.5 + (5. / 50) * i);
  }
  // create an instance of the Data class. Here the variables
  // are linked with the tree using the SetBranchAddress method
  m_data = new Data(m_chain);

  // decare Harmonic objects
  m_num_harmonics = 5;
  m_num_ptBins = pt_range.size();
  m_num_centralityBins = 12;
/*   m_num_VnCentralityBins = 6;
  for (int i = 0; i < 12; i++)
  {
    if (i < 2)
      m_centMap.insert({i, 0});
    else if (i < 3)
      m_centMap.insert({i, 1});
    else if (i < 4)
      m_centMap.insert({i, 2});
    else if (i < 6)
      m_centMap.insert({i, 3});
    else if (i < 8)
      m_centMap.insert({i, 4});
    else
      m_centMap.insert({i, 5});
  } */

  m_num_VnCentralityBins = 4;
  for (int i = 0; i < 10; i++)
  {
    if (i < 1)
      m_centMap.insert({i, 0});
    else if (i < 5)
      m_centMap.insert({i, 1});
    else if (i < 9)
      m_centMap.insert({i, 2});
    else
      m_centMap.insert({i, 3});
  }

  m_num_EtaDivs = 2;
  m_ionDirection = true;
  m_harmonics = new std::vector<Harmonic *>();
  m_harmonics->reserve(400);

  for (int i = 0; i < m_num_harmonics; i++)
  {
    for (int j = 0; j < m_num_ptBins; j++)
    {
      for (int k = 0; k < m_num_VnCentralityBins; k++)
      {
        m_harmonics->push_back(new Harmonic(i + 2, j, k));
      }
    }
  }
  
  m_resolutions = new std::vector<Resolution *>();
  m_resolutions->reserve(20);
  for(int i = 0; i< m_num_harmonics; i++)
  {
    for(int k=0; k<m_num_VnCentralityBins; k++){
      m_resolutions->push_back(new Resolution(i+2,k));
    }
  }
  
  
  
  
  
  m_meanQvectors = new std::vector<MeanQvector *>();
  m_meanQvectors->reserve(100);
  for (int i = 0; i < m_num_harmonics; i++)
  {
    for (int j = 0; j < m_num_EtaDivs; j++)
    {
      for (int k = 0; k < m_num_centralityBins; k++)
      {
        m_meanQvectors->push_back(new MeanQvector(i + 2, k, j));
      }
    }
  }

  m_weights = new std::vector<TrackCalibrationWeights *>();

  // initialise the algorithms
  for (auto algorithm : algorithms)
  {
    algorithm->initialize(m_data, m_harmonics, m_meanQvectors, m_weights, m_resolutions);
  }
  
  //Read in Calibration Objects
  std::unique_ptr<TFile> calibFile(TFile::Open(calibFileName));
  auto tree = calibFile->Get<TTree>("treeCalib");
  // helper objects
  int tempRunNum;
  TrackCalibrationWeights *Weight = 0;
  QCalibrationMeans *Means = 0;
  tree->SetBranchAddress("runNum", &tempRunNum);
  tree->SetBranchAddress("Weight", &Weight);
  tree->SetBranchAddress("Means", &Means);
  m_weights->reserve(tree->GetEntries());
  // make a deep copy of weights and means objects
  for (int i = 0; i < tree->GetEntries(); i++)
  {
    tree->GetEntry(i);
    m_runMap.insert({tempRunNum, i});
    m_weights->push_back(new TrackCalibrationWeights(*Weight));
    m_qCalibMeans.push_back(new QCalibrationMeans(*Means));
    std::cout << "Added Calibrations for Run: " << tempRunNum << std::endl;
  }
  
  auto end = std::chrono::high_resolution_clock::now();
  
  // Calculating total time taken by the program.
  double time_taken =
      std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();

  

  std::cout << "Time taken by init is : "
       << time_taken;
  std::cout << " ns" << std::endl;
}

void EventLoop::execute()
{

  // sanity check. m_chain must not be zero
  if (!m_chain)
  {
    throw std::runtime_error("Calling execute while the went loop was not initialized.");
  }

  // Should run even if runs are out of order with maps
  double Average = 0;
  double harmonicAverage = 0;
  double qAverage = 0;
  double avgTrkUsed = 0;
  double avgTrk = 0;
  double avgProp = 0;
  FlowEvent flowEvent(m_num_harmonics);

  for (int i = 0; i < m_chain->GetEntriesFast(); ++i)
  {
    auto start = std::chrono::high_resolution_clock::now();

    // read the data for i-th event
    Long64_t local_i = m_chain->LoadTree(i);

    if (local_i == 0)
    {
      std::cout << "Resetting Branch for Reading Next TFile" << std::endl;
      m_data->ResetBranch(m_chain);
    }
    
    if (i % 1000000 == 0)
    {
      std::cout << "2nd Loop: " << i << std::endl;
    }

    // branches need local index, from its own TFile
    m_data->b_FCal_Et->GetEntry(local_i);
    m_data->b_FCal_Et_P->GetEntry(local_i);
    m_data->b_FCal_Et_N->GetEntry(local_i);
    m_data->b_runNumber->GetEntry(local_i);
    m_data->b_centrality->GetEntry(local_i);
     
    auto cent = m_data->centrality;
    auto runNum = m_data->runNumber;
    
    if (cent < 0 || m_data->FCal_Et < 0 || m_data->FCal_Et_N < 0 || m_data->FCal_Et_P < 0)
      continue;

    m_data->b_FCal_Qx->GetEntry(local_i);
    m_data->b_FCal_Qy->GetEntry(local_i);

    flowEvent.ConstructFlowEvent(runNum, m_data->FCal_Qx,
                        m_data->FCal_Qy, m_qCalibMeans.at(m_runMap[runNum])->AvgQ_Re(cent),
                        m_qCalibMeans.at(m_runMap[runNum])->AvgQ_Im(cent), m_data->FCal_Et_P, m_data->FCal_Et_N);
    for(int i = 0; i<m_num_harmonics; i++){
      m_resolutions->at(m_centMap[cent] + m_num_VnCentralityBins * i)->AddRes(flowEvent.getResolutionEstimate(i));
    }
    auto qstart = std::chrono::high_resolution_clock::now();
    m_data->b_track_pt->GetEntry(local_i);
    m_data->b_track_eta->GetEntry(local_i);
    m_data->b_track_phi->GetEntry(local_i);
    auto qend = std::chrono::high_resolution_clock::now();

    auto harmstart = std::chrono::high_resolution_clock::now();
    
    int trkCounter = 0;
     for (int m = 0; m < m_data->track_pt->size(); m++)
    {
      
      auto eta = m_data->track_eta->at(m);
      
      // here we deal with tracks that dont satisfy the rapidity gap requierment
      if (!std::signbit(eta) == m_ionDirection) // && fabs(eta) > 1.7 )
        continue;
      trkCounter++;
      
      auto phi = m_data->track_phi->at(m);
      int ptBinsIndex = std::distance(pt_range.begin(), std::lower_bound(pt_range.begin(),
                                                                         pt_range.end(), m_data->track_pt->at(m))) - 1;

      int etaBinsIndex = std::distance(eta_range.begin(), std::lower_bound(eta_range.begin(),
                                                                           eta_range.end(), eta)) - 1;

      int phiBinsIndex = std::distance(phi_range.begin(), std::lower_bound(phi_range.begin(),
                                                                           phi_range.end(), phi)) - 1;

      // here we deal with overflow vectors
      if (ptBinsIndex == pt_range.size() || etaBinsIndex == eta_range.size() || phiBinsIndex == phi_range.size() ||
          ptBinsIndex == -1 || etaBinsIndex == -1 || phiBinsIndex == -1)
        continue;
      
      for (int j = 0; j < m_num_harmonics; j++)
      {
        
         m_harmonics->at(m_centMap[cent] + ptBinsIndex * m_num_VnCentralityBins + 
                        j * m_num_VnCentralityBins * m_num_ptBins)->
                        AddHarmonic(flowEvent.getCorrectedQVector(j, m_ionDirection), 
                        phi, m_weights->at(m_runMap[runNum])->trkWgt(phiBinsIndex, etaBinsIndex), 
                        flowEvent.getResolutionEstimate(j));  
          
      }

    }
    
    auto harmend = std::chrono::high_resolution_clock::now();

    // Save the Raw and Calibrated Q vectors
    for (int x = 0; x < m_num_harmonics; x++)
    {
      for (int y = 0; y < m_num_EtaDivs; y++)
      {
        m_meanQvectors->at(cent + y * m_num_centralityBins + x * m_num_centralityBins * m_num_EtaDivs)
            ->AddToAverage(flowEvent.getRawQVector(x, y), flowEvent.getCorrectedQVector(x, y));
      }
    }
  

    auto end = std::chrono::high_resolution_clock::now();
    Average = (Average * static_cast<double>(i) + std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count())/static_cast<double>(i+1);
    harmonicAverage = (harmonicAverage * static_cast<double>(i) + std::chrono::duration_cast<std::chrono::nanoseconds>(harmend - harmstart).count()) / static_cast<double>(i + 1);
    qAverage = (qAverage * static_cast<double>(i) + std::chrono::duration_cast<std::chrono::nanoseconds>(qend - qstart).count()) / static_cast<double>(i + 1);
    avgTrkUsed = (avgTrkUsed * static_cast<double>(i) + static_cast<double>(trkCounter)) / static_cast<double>(i + 1);
    avgTrk = (avgTrk * static_cast<double>(i) + static_cast<double>(m_data->track_pt->size())) / static_cast<double>(i + 1);
    avgProp = (avgProp * static_cast<double>(i) + static_cast<double>(trkCounter) / static_cast<double>(m_data->track_pt->size())) / static_cast<double>(i+1);
   }
   std::cout << "Tracks used to calculate flow: " << avgTrkUsed << " || Total Number of Tracks: " << avgTrk << " || Proportion of Tracks in Analysis: " << avgProp << std::endl;
   std::cout << "Avg Time taken per loop : "
             << Average;
   std::cout << "| per harmonic loop : "
             << harmonicAverage;
   std::cout << "| per q loop : "
             << qAverage;
   std::cout << " ns" << std::endl;

   
   // execute all the algorithms attached to this event loop
   for (auto algorithm : algorithms)
   {
    algorithm->executeHarmonic();
    algorithm->executeMeanQvectors();
    algorithm->executeTrackCalib();
    algorithm->executeResolutions();
   }
  
}
