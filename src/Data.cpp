#include "../include/Data.h"
Data::Data(TTree *tree) : m_tree(tree)
{
  m_tree->SetBranchStatus("*", 0); // disable all branches
  
  m_tree->SetBranchStatus("event_FCal_Et", 1); // activate branchname
  m_tree->SetBranchAddress("event_FCal_Et", &FCal_Et, &b_FCal_Et);
  m_tree->SetBranchStatus("event_FCal_Et_A", 1);
  m_tree->SetBranchAddress("event_FCal_Et_A", &FCal_Et_P, &b_FCal_Et_P);
  m_tree->SetBranchStatus("event_FCal_Et_C", 1);
  m_tree->SetBranchAddress("event_FCal_Et_C", &FCal_Et_N, &b_FCal_Et_N);
  m_tree->SetBranchStatus("event_nTrk", 1);
  m_tree->SetBranchAddress("event_nTrk", &event_nTrk, &b_event_nTrk);
  m_tree->SetBranchStatus("event_RunNumber", 1);
  m_tree->SetBranchAddress("event_RunNumber", &runNumber, &b_runNumber);
  m_tree->SetBranchStatus("event_EventNumber", 1);
  m_tree->SetBranchAddress("event_EventNumber", &eventNumber, &b_eventNumber);
  m_tree->SetBranchStatus("event_Centrality", 1);
  m_tree->SetBranchAddress("event_Centrality", &centrality, &b_centrality);
  m_tree->SetBranchStatus("event_FCal_Qx", 1);
  m_tree->SetBranchAddress("event_FCal_Qx", &FCal_Qx, &b_FCal_Qx);
  m_tree->SetBranchStatus("event_FCal_Qy", 1);
  m_tree->SetBranchAddress("event_FCal_Qy", &FCal_Qy, &b_FCal_Qy);
  m_tree->SetBranchStatus("event_track_eta", 1);
  m_tree->SetBranchAddress("event_track_eta", &track_eta, &b_track_eta);
  m_tree->SetBranchStatus("event_track_phi", 1);
  m_tree->SetBranchAddress("event_track_phi", &track_phi, &b_track_phi);
  m_tree->SetBranchStatus("event_track_pt", 1);
  m_tree->SetBranchAddress("event_track_pt", &track_pt, &b_track_pt);
}

void Data::ResetBranch(TTree *tree)
{
  b_FCal_Et  = tree->GetBranch("event_FCal_Et");
  b_FCal_Et_P  = tree->GetBranch("event_FCal_Et_A");
  b_FCal_Et_N  = tree->GetBranch("event_FCal_Et_C");
  b_event_nTrk  = tree->GetBranch("event_nTrk");
  b_runNumber  = tree->GetBranch("event_RunNumber");
  b_eventNumber  = tree->GetBranch("event_EventNumber");
  b_centrality  = tree->GetBranch("event_Centrality");
  b_FCal_Qx  = tree->GetBranch("event_FCal_Qx");
  b_FCal_Qy  = tree->GetBranch("event_FCal_Qy");
  b_track_eta  = tree->GetBranch("event_track_eta");
  b_track_phi  = tree->GetBranch("event_track_phi");
  b_track_pt  = tree->GetBranch("event_track_pt");
}
