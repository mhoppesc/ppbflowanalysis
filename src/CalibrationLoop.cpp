#include "../include/CalibrationLoop.h"
#include <iostream>
#include <stdio.h>
#include <stdexcept>
#include <bitset>
#include <chrono>
#include "TFile.h"
#include "TH1D.h"

ClassImp(TrackCalibrationWeights);
ClassImp(QCalibrationMeans) // add qcalibmeans and map

void CalibrationLoop::initialize()
{
  // create an instance of the TChain class
  m_chain = new TChain(treeName);

  // loop through the input files and add them to the chain
  for (auto inputFile : inputFiles)
  {
    m_chain->Add(inputFile);
    std::cout << "Added file: " << inputFile << std::endl;
  }

  // create an instance of the Data class. Here the variables
  // are linked with the tree using the SetBranchAddress method
  m_data = new Data(m_chain);
  
  //init global analysis configs
  m_num_harmonics = 5;
  m_num_centralityBins = 10;


}

void CalibrationLoop::execute()
{
  std::unique_ptr<TFile> myFile(TFile::Open(calibFileName, "RECREATE"));
  auto tree = std::unique_ptr<TTree>(new TTree("treeCalib", "Calibration Constants done a priori to the analysis"));
  myFile->SetBit(TFile::kDevNull);
  TH1::AddDirectory(kFALSE);
  
  TrackCalibrationWeights Weight;
  QCalibrationMeans Means;
  tree->Branch("runNum", &runNum, "runNum/I");
  tree->Branch("Weight", &Weight,32000,1);
  tree->Branch("Means", &Means,32000,1);

  // sanity check. m_chain must not be zero
  if (!m_chain)
  {
    throw std::runtime_error("Calling execute while the went loop was not initialized.");
  }
  
  // here we do the actual Calibration Loop
  for (int i = 0; i < m_chain->GetEntriesFast(); ++i)
  {
    // read the data for i-th event
    Long64_t local_i = m_chain->LoadTree(i);
    // branches need local index, from its own TFile
    if (local_i == 0){
      std::cout << "Resetting Branch for Reading Next TFile" << std::endl;
      m_data->ResetBranch(m_chain);
    }
    m_data->b_FCal_Et->GetEntry(local_i);
    m_data->b_FCal_Et_P->GetEntry(local_i);
    m_data->b_FCal_Et_N->GetEntry(local_i);
    m_data->b_runNumber->GetEntry(local_i);
    m_data->b_centrality->GetEntry(local_i);
    if (i % 1000000 == 0)
    {
      std::cout << "1st Loop: " << i << std::endl;
    }


    if (m_data->centrality < 0 || m_data->FCal_Et < 0 || m_data->FCal_Et_N < 0 || m_data->FCal_Et_P < 0) // Future make cut object that takes m_data as an argument and keeps track of cuts
      continue;

    m_data->b_FCal_Qx->GetEntry(local_i);
    m_data->b_FCal_Qy->GetEntry(local_i);
    m_data->b_track_eta->GetEntry(local_i);
    m_data->b_track_phi->GetEntry(local_i);

    if (!m_runMap.count(m_data->runNumber))
    {
      // new run
      m_runMap.insert({m_data->runNumber, m_weights.size()});
      m_weights.push_back(new TrackCalibrationWeights(m_data->runNumber));
      m_qCalibMeans.push_back(new QCalibrationMeans(m_data->runNumber, m_num_centralityBins, m_num_harmonics));
    }
    // meat of loop
    m_weights.at(m_runMap[m_data->runNumber])->AddElements(m_data->track_eta, m_data->track_phi);
    m_qCalibMeans.at(m_runMap[m_data->runNumber])->AddElements(m_data->FCal_Qx, m_data->FCal_Qy, m_data->centrality, m_data->FCal_Et_P, m_data->FCal_Et_N);
  }

  for(int i = 0; i<m_weights.size(); i++){
    runNum = m_weights.at(i)->getRunNumber();
    m_weights.at(i)->FinalizeElements();
    m_weights.at(i)->trackWeight().Write();
    m_weights.at(i)->trackMapInit().Write();
    m_weights.at(i)->trackMapCorrected().Write();
    Weight = *(m_weights.at(i));
    m_qCalibMeans.at(i)->FinalizeElements();
    Means = *(m_qCalibMeans.at(i));
    for(int harmonicCounter = 0; harmonicCounter < m_num_harmonics; harmonicCounter++){
      for(int centralityCounter = 0; centralityCounter < m_num_centralityBins; centralityCounter++){
        m_qCalibMeans.at(i)->QnP_Re(harmonicCounter,centralityCounter)->Write();
        m_qCalibMeans.at(i)->QnP_Im(harmonicCounter, centralityCounter)->Write();
        m_qCalibMeans.at(i)->QnN_Re(harmonicCounter, centralityCounter)->Write();
        m_qCalibMeans.at(i)->QnN_Im(harmonicCounter, centralityCounter)->Write();
      }
    }
    tree->Fill();
    std::cout << "Wrote Calibrations for Run: " << runNum << std::endl;
  }
   tree->Write();
}
