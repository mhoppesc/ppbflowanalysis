#include "../include/Resolution.h"

Resolution::Resolution(int n, int centralityBin)
{ // class def
  m_resolutionHist = new TH1D(Form("Resolutionv%d_CentralityClass_%d", n, centralityBin),
                              Form("Resolutionv%d_CentralityClass_%dl;v%d;counts", n, centralityBin, n), 100, 0, 0);
  m_n = n;
  m_centralityBin = centralityBin;
  m_lastHarm = 0;
}

void Resolution::AddRes(float Normalization)
{
  TH1::StatOverflows(kTRUE);
  m_resolutionHist->Fill(Normalization);
}



