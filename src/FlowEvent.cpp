#include "../include/FlowEvent.h"

void FlowEvent::ConstructFlowEvent(UInt_t runNumber, std::vector<float> *FCal_Qx_Raw, std::vector<float> *FCal_Qy_Raw, std::vector<float> *FCal_Qx_Mean, std::vector<float> *FCal_Qy_Mean, float FCal_Et_P, float FCal_Et_N)
{
  //means are taken from the correct centrality bin already 
  m_runNumber = runNumber;

  for (int i = 0; i < m_num_flowHarmonics; i++)
  {
    m_Q_P.at(i) = TComplex((FCal_Qx_Raw->at(i) / FCal_Et_P - FCal_Qx_Mean->at(i)), (FCal_Qy_Raw->at(i) / FCal_Et_P - FCal_Qy_Mean->at(i)));
    m_Q_N.at(i) = TComplex((FCal_Qx_Raw->at(i + m_num_flowHarmonics) / FCal_Et_N - FCal_Qx_Mean->at(i + m_num_flowHarmonics)),
                           (FCal_Qy_Raw->at(i + m_num_flowHarmonics) / FCal_Et_N - FCal_Qy_Mean->at(i + m_num_flowHarmonics)));
    m_Qraw_P.at(i) = TComplex(FCal_Qx_Raw->at(i) / FCal_Et_P, FCal_Qy_Raw->at(i) / FCal_Et_P);
    m_Qraw_N.at(i) = TComplex(FCal_Qx_Raw->at(i + m_num_flowHarmonics) / FCal_Et_N, FCal_Qy_Raw->at(i + m_num_flowHarmonics) / FCal_Et_N);
    m_resolutionEstimate.at(i) = m_Q_P.at(i).Re()* m_Q_N.at(i).Re()+m_Q_P.at(i).Im()* m_Q_N.at(i).Im();
  }

}

TComplex FlowEvent::getCorrectedQVectorConj(int n, bool isN)
{
  if(isN)
    return TComplex::Conjugate(m_Q_N.at(n));
  return TComplex::Conjugate( m_Q_P.at(n) );
}

TComplex FlowEvent::getCorrectedQVector(int n, bool isN)
{
  if (isN)
    return m_Q_N.at(n);
  return m_Q_P.at(n);
}

TComplex FlowEvent::getRawQVectorConj(int n, bool isN)
{
  if (isN)
    return TComplex::Conjugate(m_Qraw_N.at(n));
  return TComplex::Conjugate(m_Qraw_P.at(n));
}

TComplex FlowEvent::getRawQVector(int n, bool isN)
{
  if (isN)
    return m_Qraw_N.at(n);
  return m_Qraw_P.at(n);
}

double FlowEvent::getResolutionEstimate(int n)
{
  return m_resolutionEstimate.at(n);
}
