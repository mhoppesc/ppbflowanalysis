#include "../include/Harmonic.h"

Harmonic::Harmonic(int n, int ptBin, int centralityBin){ // class def
  
  m_harmonicHist = new TH1D(Form("Numeratorv%d_ptBin_%d_CentralityClass_%d",n,ptBin,centralityBin),
                                Form("Numeratorv%d_ptBin_%d_CentralityClass_%dl;v%d;counts",n,ptBin,centralityBin,n), 100, 0, 0);
  m_resolutionHist = new TH1D(Form("Resolutionv%d_ptBin_%d_CentralityClass_%d", n, ptBin, centralityBin),
                              Form("Resolutionv%d_ptBin_%d_CentralityClass_%dl;v%d;counts", n, ptBin, centralityBin, n), 100, 0, 0);
  m_harmonicHistEP = new TH1D(Form("v%d_ptBin_%d_CentralityClass_%d_EP", n, ptBin, centralityBin),
                                     Form("v%d_ptBin_%d_CentralityClass_%d_EP;v%d;counts", n, ptBin, centralityBin, n), 100, 0., 0.);
  m_n = n;
  m_ptBin = ptBin;
  m_centralityBin = centralityBin;
  m_lastHarm = 0;
}

void Harmonic::AddHarmonic(TComplex bigQ_n, float phi, float weight, float Normalization){
  TH1::StatOverflows(kTRUE);
  m_harmonicHist->Fill((TMath::Cos(static_cast<float>(m_n) * phi) * bigQ_n.Re() + TMath::Sin(static_cast<float>(m_n) * phi) * bigQ_n.Im()), weight);
  m_resolutionHist->Fill(Normalization);
}

TComplex Harmonic::smallQ(float phi, float weight){
  return TComplex::Exp(TComplex::I() * static_cast<double>(static_cast<float>(m_n)*phi));
}

void Harmonic::AddHarmonicEP(TComplex bigQ_n){
  m_harmonicHistEP->Fill((bigQ_n.Re()*bigQ_n.Re()+bigQ_n.Im()*bigQ_n.Im()));
}
